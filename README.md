# GNOME Cats

Simple GTK3 application that brings you cats to your desktop.

<img style="align: center" src="https://files.mastodon.social/media_attachments/files/113/880/096/431/724/373/original/985e47f8f11d5326.png" />

## Install

    $ git clone https://gitlab.com/JuanjoSalvador/gnome-cats.git && cd gnome-cats
    $ pip3 install ./gnome-cats
    $ gnome-cats

## Development

    $ make install
    $ make run
